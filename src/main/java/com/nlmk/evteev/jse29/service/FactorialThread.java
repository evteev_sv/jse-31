package com.nlmk.evteev.jse29.service;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FactorialThread implements Runnable {

    private final Map<String, List<Integer>> threadRange = new HashMap<>();
    private BigInteger factorialResult = BigInteger.ONE;

    @Override
    public void run() {
        List<Integer> range = threadRange.get(Thread.currentThread().getName());
        calcRangeFactorial(range.get(0), range.get(1));
    }

    private void calcRangeFactorial(int start, int end) {
        BigInteger result = BigInteger.ONE;
        for (int i = start; i <= end; i++) {
            result = result.multiply(BigInteger.valueOf(i));
        }
        multiplyPart(result);
    }

    private synchronized void multiplyPart(BigInteger part) {
        factorialResult = factorialResult.multiply(part);
    }

    public BigInteger getFactorialResult() {
        return factorialResult;
    }

    public void addThreadRange(String threadName, int start, int end) {
        threadRange.put(threadName, Arrays.asList(start, end));
    }

    public void clearThreadRange() {
        threadRange.clear();
    }
}