package com.nlmk.evteev.jse29.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class NumericService {

    private final List<Thread> threads = new ArrayList<>();

    /**
     * Умножение чисел
     *
     * @param var1 строковое представление числа
     * @param var2 строковое представление числа
     * @return результат умножения
     * @throws IllegalArgumentException ошибка при переполнении long
     *                                  или неверном типе аргумента
     */
    public long multiplication(String var1, String var2) throws IllegalArgumentException {
        if ((var1 == null || var1.trim().isEmpty())
                || (var2 == null || var2.trim().isEmpty())) {
            throw new IllegalArgumentException();
        }
        int arg1;
        int arg2;
        try {
            arg1 = Integer.parseInt(var1);
            arg2 = Integer.parseInt(var2);
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException(ne.getMessage());
        }
        long result;
        try {
            result = Math.multiplyExact(arg1, arg2);
        } catch (ArithmeticException ae) {
            throw new IllegalArgumentException(ae.getMessage());
        }
        return result;
    }

    /**
     * Вычисление факториала числа
     *
     * @param arg строковое представление числа
     * @return результат вычислений факториала
     * @throws IllegalArgumentException ошибка при переполнении числа
     *                                  или неверном типе аргумента
     */
    public BigInteger factorial(String arg, Integer threadCount) throws IllegalArgumentException {
        if (arg == null || arg.trim().isEmpty()) {
            throw new IllegalArgumentException("Аргумент не может быть пустым!");
        }
        if (threadCount < 1) {
            throw new IllegalArgumentException("Кол-во потоков не может быть < 1!");
        }
        long startTime = System.currentTimeMillis();
        threads.clear();
        int n;
        try {
            n = Integer.parseInt(arg);
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException(ne.getMessage());
        }
        if (n == 0) {
            return BigInteger.ONE;
        }
        FactorialThread factorial = new FactorialThread();
        createFctorialThread(threadCount, factorial);
        for (int i = 0; i < threadCount; i++) {
            int start = i * (n / threadCount) + 1;
            int end = (i + 1) * (n / threadCount);
            if ((i == threadCount - 1) && (end != n)) {
                end += n - end;
            }
            factorial.addThreadRange(threads.get(i).getName(), start, end);
        }
        for (Thread thread : threads) {
            thread.start();
        }
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        long endTime = System.currentTimeMillis();
        System.out.println("Расчет длился " + (endTime - startTime) + " ms");
        factorial.clearThreadRange();
        return factorial.getFactorialResult();
    }

    /**
     * Создание потока для расчета факториала
     *
     * @param value           кол-во потоков
     * @param factorialThread класс, реализующий интерфейс {@link Runnable}
     */
    private void createFctorialThread(int value, FactorialThread factorialThread) {
        for (int i = 0; i < value; i++) {
            threads.add(new Thread(factorialThread));
        }
    }

    /**
     * Разложение числа на ряд Фибоначчи
     *
     * @param arg строковое представление числа
     * @return массив чисел, представляющий ряд Фибоначчи
     * @throws IllegalArgumentException ошибка при невозможности разложения числа
     *                                  или неверном типе аргумента
     */
    public long[] fibonacci(String arg) throws IllegalArgumentException {
        if (arg == null || arg.trim().isEmpty()) {
            throw new IllegalArgumentException();
        }
        int value;
        try {
            value = Integer.parseInt(arg);
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException(nfe.getMessage());
        }
        if (value < 0) {
            throw new IllegalArgumentException("Значение параметра < 0!");
        }
        long[] result = new long[value + 1];
        if (result.length == 1) {
            result[0] = 0;
            return result;
        }
        if (result.length == 2) {
            result[0] = 0;
            result[1] = 1;
            return result;
        }
        result[0] = 0;
        result[1] = 1;
        for (int i = 2; i < result.length; i++) {
            result[i] = result[i - 1] + result[i - 2];
        }
        return result;
    }

}
